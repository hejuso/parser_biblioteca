package parser.biblioteca;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

public class Parser {
	
	private Document dom = null;
	private ArrayList<Libro> libros = null;
	
	public Parser() {
		libros = new ArrayList<Libro>();
	}
	
	public void parseFicheroXml(String fichero) {
		
		// Creamos una factory		
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

		
		try {
			// Se crea un document builder
			DocumentBuilder db = dbf.newDocumentBuilder();
			
			//Se parsea el xml y obtenemos el DOM
			dom = db.parse(fichero);
			
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException se) {
			se.printStackTrace();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
		
	}
	
	public void parseDocument() {
		//Se obtiene el elemento raiz
		Element docEle = dom.getDocumentElement();
		
		// Se obtiene la lista de nodos de elementos
		
		NodeList nl = docEle.getElementsByTagName("libro");
		
		if(nl != null && nl.getLength() > 0) {
			for(int i=0; i < nl.getLength(); i++) {
				
				//Obtenemos elemento de la lista (libro)
				Element el = (Element) nl.item(i);
				
				//obtenemos un libro
				Libro l = getLibro(el);
				
				// A�adimos el libro al array
				libros.add(l);
			}
		}
		
	}

	private Libro getLibro(Element LibroElem) {
		// Para cada elemento libro obrenemos sus datos
		
		String titulo = getTextValue(LibroElem, "titulo");
		ArrayList<String> autor = getArrValue(LibroElem, "nombre");

		int paginas = getIntValue(LibroElem, "paginas");
		int anyo = getAttributeIntValue(LibroElem,"titulo", "anyo");
		String editor = getTextValue(LibroElem, "editor");

		// Creamos el objeto libro a partir de los datos recibidos
		Libro e = new Libro(titulo,autor,anyo,editor,paginas);
		
		return e;
		
	}
	
	private String getTextValue(Element ele, String tagName) {
		
		String textVal = null;
		NodeList nl = ele.getElementsByTagName(tagName);
		if(nl != null && nl.getLength() > 0) {
			Element el = (Element)nl.item(0);
			textVal = el.getFirstChild().getNodeValue();
		}
		
		return textVal;
		
	}
	
	private int getIntValue(Element ele, String tagName) {
		
		return Integer.parseInt(getTextValue(ele,tagName));
		
	}
	
	private int getAttributeIntValue(Element ele, String tagName, String attrName) {
		
		int value = 0;
		NodeList nl = ele.getElementsByTagName(tagName);
		if(nl != null && nl.getLength() > 0) {
			Element el = (Element)nl.item(0);
			value = Integer.parseInt(el.getAttribute(attrName));
		}
		
		return value;
		
	}
	
	private ArrayList<String> getArrValue(Element ele, String tagName) {
		
		String textVal = null;
		NodeList subNode = null;
		ArrayList<String> autorNombres = new ArrayList<String>();
		NodeList nl = ele.getElementsByTagName(tagName);
		
		if(nl != null && nl.getLength() > 0) {
			for(int i=0; i < nl.getLength(); i++) {
				
				//Obtenemos elemento de la lista (libro)
				Element el = (Element) nl.item(i);
				autorNombres.add(el.getTextContent());
			}
		}

		
		return autorNombres;
		
	}
	
	public void print() {
		Iterator it = libros.iterator();
		while(it.hasNext()) {
			Libro l = (Libro) it.next();
			l.print();
		}
	}

}
