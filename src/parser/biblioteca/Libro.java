package parser.biblioteca;

import java.util.ArrayList;

public class Libro {

	private String titulo = null;
	private ArrayList<String> autor = null;

	private int anyo = 0;
	private String editor = null;
	private int paginas = 0;

	public Libro() {

	}

	public Libro(String tit, ArrayList<String> aut, int y, String edit, int pag) {

		titulo = tit;
		autor = aut;
		anyo = y;
		editor = edit;
		paginas = pag;

	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public int getAnyo() {
		return anyo;
	}

	public void setAnyo(int anyo) {
		this.anyo = anyo;
	}

	public String getEditor() {
		return editor;
	}

	public void setEditor(String editor) {
		this.editor = editor;
	}

	public int getPaginas() {
		return paginas;
	}

	public void setPaginas(int paginas) {
		this.paginas = paginas;
	}
	
	public ArrayList<String> getAutor() {
		return autor;
	}

	public void setAutor(ArrayList<String> autor) {
		this.autor = autor;
	}

	public void print() {
		System.out.println("Titulo: "+titulo);
		for (int i = 0; i < autor.size(); i++) {
			System.out.println("Autor: "+autor.get(i));
		}
		System.out.println("A�o: "+anyo);
		System.out.println("Editor: "+editor);
		System.out.println("Paginas: "+paginas);
		System.out.println("    ");
		System.out.println("-----------------");
		System.out.println("    ");

	}
	
	

}
